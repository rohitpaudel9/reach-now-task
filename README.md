
#Project Details:

## Description

This Project was scaffolded using [Nest](https://github.com/nestjs/nest) framework TypeScript starter repository. More project description to be added ...

## Build the Docker Image

```bash
docker build -t reach-now-task .
```
## Start Docker Daemon client
Please start your Docker Daemon client in your machine. Make sure you have logined to docker client and have access to the docker resources.

## Run the image

```
docker run -p 3000:3000 reach-now-task
```
## Check in the Browser
 http://localhost:3000/ 

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

  Nest is [MIT licensed](LICENSE).
